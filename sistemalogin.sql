-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Tempo de geração: 20-Maio-2020 às 14:38
-- Versão do servidor: 10.4.10-MariaDB
-- versão do PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `sistemalogin`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `pergunta`
--

DROP TABLE IF EXISTS `pergunta`;
CREATE TABLE IF NOT EXISTS `pergunta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titulo` varchar(25) COLLATE utf8_bin NOT NULL,
  `pergunta` varchar(250) COLLATE utf8_bin NOT NULL,
  `data` datetime DEFAULT current_timestamp(),
  `usuario` varchar(25) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Extraindo dados da tabela `pergunta`
--

INSERT INTO `pergunta` (`id`, `titulo`, `pergunta`, `data`, `usuario`) VALUES
(2, 'teste', 'perguntaaaaaaaaaaa', '2020-05-19 20:51:11', 'jonny2'),
(3, 'titlu', 'umna perguntinha', '2020-05-19 20:57:28', 'jonny2'),
(4, 'testandoaaaa', 'aaaaaaaaaaaaaaa', '2020-05-19 21:03:56', 'anÃ´nimo'),
(5, 'testando', 'ayyyyyy', '2020-05-19 21:05:13', 'adrian777'),
(6, 'dados atuais', 'quais sÃ£o o numero de mortes agora?', '2020-05-20 10:49:55', 'anÃ´nimo'),
(7, 'que dia Ã© hoje?', 'eu estava querendo saber que dia Ã© hoje', '2020-05-20 11:23:47', 'anÃ´nimo');

-- --------------------------------------------------------

--
-- Estrutura da tabela `resposta`
--

DROP TABLE IF EXISTS `resposta`;
CREATE TABLE IF NOT EXISTS `resposta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `perguntaid` int(11) NOT NULL,
  `data` datetime NOT NULL DEFAULT current_timestamp(),
  `usuario` varchar(25) NOT NULL,
  `resposta` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `resposta`
--

INSERT INTO `resposta` (`id`, `perguntaid`, `data`, `usuario`, `resposta`) VALUES
(1, 3, '2020-05-19 23:26:56', 'adrian777', 'acho que esta tudo errado'),
(2, 2, '2020-05-19 23:30:14', 'adrian777', 'chupa minha cactgfiuyh'),
(3, 6, '2020-05-20 11:16:39', 'anÃ´nimo', 'simmmm porquem sim'),
(4, 6, '2020-05-20 11:16:51', 'anÃ´nimo', 'acho que nao porque nao'),
(5, 7, '2020-05-20 11:24:21', 'anÃ´nimo', 'nÃ£o sei que dia Ã© hoje meu mano'),
(6, 7, '2020-05-20 11:25:01', 'adrian777', 'eu sei que dia Ã© hoje mano');

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuario`
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `login` varchar(80) NOT NULL,
  `senha` varchar(32) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `login`, `senha`) VALUES
(3, 'Joao Victor Correia de Oliveira', 'admin', 'e10adc3949ba59abbe56e057f20f883e'),
(7, 'aaa', 'aaaaaa', '12345'),
(6, 'adriana', 'adrian777', '123456'),
(8, 'joao vicrto', 'jonnyzin', '777'),
(9, 'jajaac da silva', 'zupa', '12345678');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
