<?php 
require_once 'conexaodb.php'; // Chmando a página do banco

//Sessão
session_start();



//Botão form
if(isset($_POST['cria'])){
    $mensagens = [];
    $nome = mysqli_escape_string($connect, $_POST["Nome"]);//Função do mysql para filtragem dos dados digitados pelo user
    $user = mysqli_escape_string($connect, $_POST['User']);//Função do mysql para filtragem dos dados digitados pelo user
    $senha = mysqli_escape_string($connect, $_POST['Senha']);//Função do mysql para filtragem dos dados digitados pelo user

    if(empty($nome) or empty($senha) or empty($user) ){ //Checando se os campos estão vazios
        $mensagens [] = 'Existem campos em branco';
    }
    else{
      if(isset($_POST['checkbox'])){
        $sql = "INSERT INTO `usuario` (`id`, `nome`, `login`, `senha`) VALUES (NULL, '".$nome."', '".$user."', '".$senha."');"; //Consulta SQL
        if ($connect->query($sql) === TRUE) {
          $mensagens [] = "Usuário criado com sucesso. Você será direcionado para o home.";
          sleep(5);
          header('Location: index.php');
      } else {
          $mensagens [] = "Error: " . $sql . "<br>" . $connect->error;
      }
      }
        else{
          $mensagens [] = "Você deve ter 18 anos para continuar, caso tenha marque na caixa.";
        }
        
    }
    $connect->close();
}
?>
<!doctype html>
<html lang="pt-br">

<head>
     <title>Corona Fórum</title>
     <link rel="stylesheet" href="..\node_modules\bootstrap\dist\css\bootstrap.min.css" crossorigin="anonymous">
     <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <link href="..\css\login.css" rel="stylesheet">

</head>

<body >
     <form class="form-signin" method="post">
  
  <div class="form-group">
    <label for="formGroupExampleInput">Nome:</label>
    <input type="text" class="form-control"  name="Nome" placeholder="João Silva Silva">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Nome de usuário:</label>
    <input type="text" class="form-control" name="User" placeholder="usuario777">
  </div>
  <div class="form-group">
    <label for="formGroupExampleInput2">Senha:</label>
    <input type="password" class="form-control"  name="Senha" placeholder="********">
    <small id="passwordHelpInline" class="text-muted">
      Deve ter no mínimo 8 caracteres.
    </small>
    <div class="form-group row">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
      <div class="form-check" >
        <input class="form-check-input" type="checkbox" id="gridCheck1" name="checkbox">
        <label class="form-check-label" for="gridCheck1" >
          *Declaro ter mais de 18 anos
        </label>
      </div>
    </div>
  </div>
  </div>
  <button class="btn btn-lg btn-success btn-block" type="submit" name="cria">Criar conta</button>

   <?php 
         if(!empty($mensagens)){
             foreach ($mensagens as $msg) {
                 echo $msg;
             }
         }
         ?>
   
     </form>
  
</body>

</html>