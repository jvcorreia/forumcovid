<?php
require_once 'conexaodb.php'; // Chmando a página do banco

//Sessão
session_start();

if (isset($_SESSION['id_usuario'])) {
	$id = $_SESSION['id_usuario'];
}


if (isset($_POST['sair'])) {
	header('Refresh:0;');
	session_unset();
}
?>


<?php
//Inicio da chamada de API
$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://coronavirus-monitor.p.rapidapi.com/coronavirus/worldstat.php",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_SSL_VERIFYHOST => false, //Essas configurações não devem ser usadas em produção 
	CURLOPT_SSL_VERIFYPEER => false, //Essas configurações não devem ser usadas em produção
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	CURLOPT_HTTPHEADER => array(
		"x-rapidapi-host: coronavirus-monitor.p.rapidapi.com",
		"x-rapidapi-key: e80c8b91a5msh049ddc8660485afp10a2b4jsn313a13eea3c7"
	),
));

$response = curl_exec($curl); //Resposta com os dados
$err = curl_error($curl);


curl_close($curl); //Fim da chamada de API


if ($err) {
	echo "Algum erro aconteu:" . $err;
} else {
	$obj_global_cases = json_decode($response);
	$global_cases = $obj_global_cases->{'total_cases'}; //Chamando o dado para mostrar no index
	$ultimaattcasos = $obj_global_cases->{'statistic_taken_at'};
}


$curl = curl_init();

curl_setopt_array($curl, array(
	CURLOPT_URL => "https://newsapi.org/v2/everything?q=covid&language=pt&apiKey=8bed67b6d1484445a27c2d638610c42b",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_SSL_VERIFYHOST => false, //Essas configurações não devem ser usadas em produção 
	CURLOPT_SSL_VERIFYPEER => false, //Essas configurações não devem ser usadas em produção
	CURLOPT_TIMEOUT => 30,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
));

$resposta = curl_exec($curl); //Resposta com os dados
$err = curl_error($curl);

curl_close($curl); //Fim da chamada de API


if ($err) {
	echo "Algum erro aconteu:" . $err;
} else {
	$obj_noticias = json_decode($resposta);
	$status = $obj_noticias->{'articles'};
	$lista_primeiros = array_slice($status, 0, 10);
}

function noticias($lista_primeiros)
{
	for ($i = 0; $i < 10; $i++) {

		print_r('<a href="' . $lista_primeiros[$i]->{'url'} . '" class="list-group-item list-group-item-action">
		<div class="d-flex w-100 justify-content-between">
		  <h5 class="mb-1">' . $lista_primeiros[$i]->{'title'} . '</h5>
		  <small class="text-muted">Clique para ver mais.</small>
		</div>
		<p class="mb-1">' . $lista_primeiros[$i]->{'description'} . '</p>
		<small class="text-muted">' . $lista_primeiros[$i]->{'author'} . ' - </small>
		<small class="text-muted">Publicado em: ' . $lista_primeiros[$i]->{'publishedAt'} . '</small>
	  </a>');
	}
}

/*
Parâmetros que podem ser usados->
		print_r($lista_primeiros[$i]->{'author'});
		print_r($lista_primeiros[$i]->{'title'});
		print_r($lista_primeiros[$i]->{'description'});
		print_r($lista_primeiros[$i]->{'url'});
		print_r($lista_primeiros[$i]->{'urlToImage'});
		*/


?>


<!doctype html>
<html lang="pt-br">

<head>
	<title>Corona Fórum</title>
	<link rel="stylesheet" href="..\node_modules\bootstrap\dist\css\bootstrap.min.css" crossorigin="anonymous">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>

<body style="background-color:#F5F5F5">
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="#">Corona Fórum</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="#">Notícias</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Fórum
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">

						<a class="dropdown-item" href=".\perguntas.php">Fazer uma pergunta</a>
						<a class="dropdown-item" href=".\verperguntas.php">Ver perguntas</a>
						<div class="dropdown-divider"></div>

					</div>
				</li>
			</ul>
			<p style="margin-right: 3%; color:white;">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {
					echo 'Bem-vindo, ' . $id;
				}
				?>
			</p>
			<form action="" method="post">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {

					echo '<button class="btn btn-outline-primary my-2 my-sm-0" type="submit" name="sair" >Sair</button>';
				} else {
					echo  '<a  class="btn btn-outline-primary my-2 my-sm-0" href=".\login.php">Login</a>';
				}
				?>

			</form>
		</div>
	</nav>
	<p style="text-align: center;border-right-style: solid;border-bottom-style: solid;border-left-style: solid; border-color:blue">
		Bem vindo ao fórum corona, o número total de casos do corona no mundo neste momento é:
		<span style="color: red"><?php echo $global_cases . '</span>, atualizado pela última vez ás:<span style="color: red"> ' . $ultimaattcasos . '</span>' ?></p>

	<div class="list-group" style="width: 70%; margin-left:15%">
		<h3 class="font-italic">Últimas notícias sobre o Covid-19</h3>
		<?php noticias($lista_primeiros) ?>
	</div>
	<footer>
		<script src="..\node_modules\jquery\dist\jquery.min.js"></script>
		<script src="..\node_modules\@popperjs\core\dist\umd\popper.min.js"></script>
		<script src="..\node_modules\bootstrap\dist\js\bootstrap.min.js"></script>
	</footer>
</body>

</html>