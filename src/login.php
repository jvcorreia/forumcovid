<?php
require_once 'conexaodb.php'; // Chmando a página do banco

//Sessão
session_start();



//Botão form
if (isset($_POST['logar'])) {
	$erros = [];
	$login = mysqli_escape_string($connect, $_POST["login"]); //Função do mysql para filtragem dos dados digitados pelo user
	$senha = mysqli_escape_string($connect, $_POST['senha']); //Função do mysql para filtragem dos dados digitados pelo user

	if (empty($login) or empty($senha)) { //Checando se os campos estão vazios
		$erros[] = 'Existem campos em branco';
	} else {
		$sql = "select login from usuario where login = '$login' "; //Consulta SQL
		$resultado = mysqli_query($connect, $sql); //Capturando o resultado
		if (mysqli_num_rows($resultado) > 0) { //Vendo se existe um resultado
			//$senha = md5($senha);
			$sql = "select * from usuario where login = '$login' and senha = '$senha' ";
			$resultado = mysqli_query($connect, $sql);

			if (mysqli_num_rows($resultado) > 0) { //Vendo se existe um resultado
				$dados = mysqli_fetch_array($resultado);
				$_SESSION['logado'] = true;
				$_SESSION['id_usuario'] = $dados['login'];
				header('Location: index.php');
			} else {
				$erros[] = 'Usuário ou senha errados';
			}
		} else {
			$erros[] = 'Usuário ou senha errados';
		} {
		}
	}
}

if (isset($_POST['cadastrar'])) {
	header('Location: cadastro.php');
}
?>

<!doctype html>
<html lang="pt-br">

<head>
	<title>Corona Fórum</title>
	<link rel="stylesheet" href="..\node_modules\bootstrap\dist\css\bootstrap.min.css" crossorigin="anonymous">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!--<link href="..\css\login.css" rel="stylesheet">-->

</head>

<body class="text-center">

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="./index.php">Corona Fórum</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="./index.php">Notícias</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Fórum
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="#">Perguntas</a>
						<a class="dropdown-item" href=".\verperguntas.php">Ver perguntas</a>
					</div>
				</li>
			</ul>
			<p style="margin-right: 3%; color:white;">
				<?php
				if (isset($id)) {
					echo 'Bem-vindo, ' . $id;
				}
				?>
			</p>
			<form action="" method="post">
				<?php
				if (isset($id)) {

					echo '<button class="btn btn-outline-primary my-2 my-sm-0" type="submit" name="sair" >Sair</button>';
				} else {
					echo  '<a  class="btn btn-outline-primary my-2 my-sm-0" href=".\login.php">Login</a>';
				}
				?>

			</form>
		</div>
	</nav>


	<div class="container">
		<form class="form-signin" method="post">
			<h1 class="h3 mb-3 font-weight-normal">Corona Fórum</h1>
			<label for="inputEmail" class="sr-only">E-mail</label>
			<input type="text" id="inputEmail" class="form-control" placeholder="exemplo@123.com" name="login" autofocus>
			<label for="inputPassword" class="sr-only">Senha</label>
			<input type="password" id="inputPassword" class="form-control" name="senha" placeholder="******">
			<div class="checkbox mb-3">
				<label>
					<input type="checkbox" value="remember-me"> Lembrar-me
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit" name="logar">Login</button>
			<button class="btn btn-outline-primary btn-block" name="cadastrar">Criar conta</button>
			<?php
			if (!empty($erros)) {
				foreach ($erros as $erro) {
					echo $erro;
				}
			}
			?>
		</form>
	</div>

</body>

<footer>
	<script src="..\node_modules\jquery\dist\jquery.min.js"></script>
	<script src="..\node_modules\@popperjs\core\dist\umd\popper.min.js"></script>
	<script src="..\node_modules\bootstrap\dist\js\bootstrap.min.js"></script>
</footer>

</html>