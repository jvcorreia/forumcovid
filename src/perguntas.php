<?php

require_once 'conexaodb.php';

session_start();

if (isset($_SESSION['id_usuario'])) {
	$id = $_SESSION['id_usuario'];
} else {
	$id = 'anônimo';
}


if (isset($_POST['perguntar'])) {
	$titulo = mysqli_escape_string($connect, $_POST["titulo"]); //Função do mysql para filtragem dos dados digitados pelo user
	$pergunta = mysqli_escape_string($connect, $_POST['pergunta']); //Função do mysql para filtragem dos dados digitados pelo user
	$sql = "INSERT INTO `pergunta` (`id`, `titulo`, `pergunta`, `data`, `usuario`)
     VALUES (NULL,'" . $titulo . "', '" . $pergunta . "', current_timestamp(), '" . $id . "')";
	if ($connect->query($sql) === TRUE) {
		echo "<script>alert('Pergunta feita com sucesso!')</script>";
	} else {
		echo "Error: " . $sql . "<br>" . $connect->error;
	}
}


?>

<!doctype html>
<html lang="pt-br">

<head>
	<title>Corona Fórum</title>
	<link rel="stylesheet" href="..\node_modules\bootstrap\dist\css\bootstrap.min.css" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="./css/perguntas.css" />
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
</head>

<body style="background-color:#F5F5F5">
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="./index.php">Corona Fórum</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="./index.php">Notícias</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Fórum
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">

						<a class="dropdown-item" href=".\perguntas.php">Fazer uma pergunta</a>
						<a class="dropdown-item" href=".\verperguntas.php">Ver perguntas</a>
						<div class="dropdown-divider"></div>

					</div>
				</li>
			</ul>
			<p style="margin-right: 3%; color:white;">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {
					echo 'Bem-vindo, ' . $id;
				}
				?>
			</p>
			<form action="" method="post">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {

					echo '<button class="btn btn-outline-primary my-2 my-sm-0" type="submit" name="sair" >Sair</button>';
				} else {
					echo  '<a  class="btn btn-outline-primary my-2 my-sm-0" href=".\login.php">Login</a>';
				}
				?>

			</form>
		</div>
	</nav>

	<div class="container contact-form">

		<form method="post">

			<h3>Faça uma Pergunta</h3>

			<div class="row">
				<div class="col-md-6">
					<div class="form-group">
						<input type="text" name="titulo" class="form-control" placeholder="Titulo*" value="" />
					</div>
					<div class="form-group">
						<input type="submit" name="perguntar" class="btn btn-lg btn-primary btn-block" value="Enviar!" />
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<textarea name="pergunta" class="form-control" placeholder="Sua pergunta" style="width: 100%; height: 150px;"></textarea>
					</div>
				</div>
			</div>
		</form>
	</div>


	<footer>
		<script src="..\node_modules\jquery\dist\jquery.min.js"></script>
		<script src="..\node_modules\@popperjs\core\dist\umd\popper.min.js"></script>
		<script src="..\node_modules\bootstrap\dist\js\bootstrap.min.js"></script>
	</footer>
</body>

</html>