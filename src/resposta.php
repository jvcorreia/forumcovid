<?php
$idpergunta = htmlspecialchars($_GET["id"]);

require_once 'conexaodb.php';

session_start();

if (isset($_SESSION['id_usuario'])) {
	$id = $_SESSION['id_usuario'];
} else {
	$id = 'anônimo';
}



if (isset($_POST["responder"])) {
	$resposta = mysqli_escape_string($connect, $_POST["resposta"]);
	$sql = "INSERT INTO `resposta` (`id`, `perguntaid`, `data`, `usuario`, `resposta`)
     VALUES (NULL,'" . $idpergunta . "', current_timestamp(), '" . $id . "','" . $resposta . "')";
	if ($connect->query($sql) === TRUE) {
		echo "<script>alert('Resposta criada com sucesso!')</script>";
	} else {
		echo "Error: " . $sql . "<br>" . $connect->error;
	}
}
?>
<?php
$result = mysqli_query($connect, "SELECT id, titulo, pergunta, data, usuario FROM pergunta WHERE id = '" . $idpergunta . "'");
if (!$result) {
	echo 'Could not run query: ';
	exit;
}
$row = mysqli_fetch_row($result);

$titulo = $row[1];
$pergunta = $row[2];
$datapergunta = $row[3];
$usuariopergunta = $row[4];
?>
<html>

<head>
	<title>Corona Fórum</title>
	<link rel="stylesheet" href="..\node_modules\bootstrap\dist\css\bootstrap.min.css" crossorigin="anonymous">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>

<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="./index.php">Corona Fórum</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="./index.php">Notícias</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Fórum
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">
						<a class="dropdown-item" href="./perguntas.php">Perguntas</a>
					</div>
				</li>
			</ul>
			<p style="margin-right: 3%; color:white;">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {
					echo 'Bem-vindo, ' . $id;
				}
				?>
			</p>
			<form action="" method="post">
				<?php
				if (isset($id) && isset($_SESSION['id_usuario'])) {

					echo '<button class="btn btn-outline-primary my-2 my-sm-0" type="submit" name="sair" >Sair</button>';
				} else {
					echo  '<a  class="btn btn-outline-primary my-2 my-sm-0" href=".\login.php">Login</a>';
				}
				?>

			</form>
		</div>
	</nav>

	<div style='margin: 3%'>

		<h1 class="display-2 text-center"> <?php echo "Tema: " . $titulo ?></h1>
		<p class="text-center font-weight-bold">
			<?php echo "
			Pergunta: " . $pergunta . "</br>Data: " . $datapergunta . "<br>Feita por: " . $usuariopergunta
			?>
		</p><br><br>

		<div class='page-header'>
			<h1><small class='pull-right'> Respostas: </small></h1>
		</div>

		<ul>
			<?php
			$sql = "SELECT `id`, `perguntaid`, `data`, `usuario`, `resposta` FROM resposta WHERE perguntaid = '" . $idpergunta . "'";
			$result = $connect->query($sql);

			if ($result->num_rows > 0) {
				// output data of each row
				while ($row = $result->fetch_assoc()) {

					echo
						"
				<div class='container pt-3 p-3 my-3 border'>
					<div class='row'>
						<div class='col-xl-8'>
							<div class='page-header'>
						</div>
						<div class='comments-list'>
							<div class='media'>
								
								<div class='media-body'>
									<h4 class='media-heading user_name'>" . $row["usuario"] . "</h4>
										" . $row["resposta"] . "				  
									<p class='pull-right'><small>Postado ás: " . $row["data"] . "</small></p>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>
				";
				}
				//<li>resposta: ". $row["resposta"] . " - usuario: " . $row["usuario"] . " " . $row["data"] . "</li><br>
			} else {
				echo "0 Respostas para essa pergunta";
			}

			?>
		</ul>

		<form action="" method="post">
			<!--<input type="text" style="height: 100px; width: 500px" name="resposta"></br>-->

			<div class="form-group">
				<label for="comment">Responder:</label>
				<textarea type="text" class="form-control" columns="3" rows="5" name="resposta"></textarea>
			</div>

			<div class="#">
				<input type="submit" name="responder" class="btn btn-lg btn-primary" value="Enviar" />
			</div>
		</form>

	</div>

</body>

<footer>
	<script src="..\node_modules\jquery\dist\jquery.min.js"></script>
	<script src="..\node_modules\@popperjs\core\dist\umd\popper.min.js"></script>
	<script src="..\node_modules\bootstrap\dist\js\bootstrap.min.js"></script>
</footer>

</html>