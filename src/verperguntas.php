<?php

require_once 'conexaodb.php';

session_start();

if (isset($_SESSION['id_usuario'])) {
	$id = $_SESSION['id_usuario'];
} else {
	$id = 'anônimo';
}


$sql = "SELECT id, titulo, pergunta, data, usuario FROM pergunta";

?>

<!doctype html>
<html lang="pt-br">

<head>
	<title>Corona Fórum</title>
	<link rel="stylesheet" href="..\node_modules\bootstrap\dist\css\bootstrap.min.css" crossorigin="anonymous">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

</head>

<body style="background-color:#F5F5F5">

	<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		<a class="navbar-brand" href="./index.php">Corona Fórum</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="./index.php">Notícias</a>
				</li>
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						Fórum
					</a>
					<div class="dropdown-menu" aria-labelledby="navbarDropdown">

						<a class="dropdown-item" href=".\perguntas.php">Fazer uma pergunta</a>
						<a class="dropdown-item" href=".\verperguntas.php">Ver perguntas</a>
						<div class="dropdown-divider"></div>

					</div>
				</li>
			</ul>
			<p style="margin-right: 3%; color:white;">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {
					echo 'Bem-vindo, ' . $id;
				}
				?>
			</p>
			<form action="" method="post">
				<?php
				if (isset($id)  && isset($_SESSION['id_usuario'])) {

					echo '<button class="btn btn-outline-primary my-2 my-sm-0" type="submit" name="sair" >Sair</button>';
				} else {
					echo  '<a  class="btn btn-outline-primary my-2 my-sm-0" href=".\login.php">Login</a>';
				}
				?>

			</form>
		</div>
	</nav>

	<ul>
		<?php

		$result = $connect->query($sql);

		if ($result->num_rows > 0) {
			// output data of each row

			echo "<div style='margin: 1%'></div>";

			while ($row = $result->fetch_assoc()) {

				echo "
				
				<div>
					
					<a href='./resposta.php?id=" . $row["id"] . "' class='list-group-item list-group-item-action flex-column align-items-start active'>
						<div class='d-flex w-100 justify-content-between'>
						  <h5 class='mb-1'>" . $row["titulo"] . "</h5>
						  <small>Postada em: " . $row["data"] . " </small>
						</div>
					 </a>
					<li class='list-group-item'>
						<p class='mb-1'>" . $row["pergunta"] . "</p> 
						<small>Feita por: " . $row["usuario"] . "</small>
					</li>
					
				</div></br>
			
			";

				/*echo "<a href='./resposta.php?id=".$row["id"]."'>"
			."id: ". $row["id"]. " - Título: " . $row["titulo"]. " " . $row["pergunta"].  " " . $row["data"]." " . $row["usuario"]."</a><br> ";*/
			}
		} else {
			echo "0 results";
		}

		?>
	</ul>

	<footer>
		<script src="..\node_modules\jquery\dist\jquery.min.js"></script>
		<script src="..\node_modules\@popperjs\core\dist\umd\popper.min.js"></script>
		<script src="..\node_modules\bootstrap\dist\js\bootstrap.min.js"></script>
	</footer>

</body>

</html>